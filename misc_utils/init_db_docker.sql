-- Run all these as the DB owner (e.g. postgres)
CREATE DATABASE mangafeedrdb;
CREATE ROLE mangafeedr_user WITH PASSWORD 'blab' LOGIN;
GRANT CONNECT ON DATABASE mangafeedrdb TO mangafeedr_user;

\c mangafeedrdb

CREATE TABLE db_settings(key text, value text);
INSERT INTO db_settings values('schema_version', 1);

-- v1
CREATE TABLE feeds
(
  id serial NOT NULL,
  name text,
  link text,
  marked_read_date timestamp with time zone,
  latest_update_date timestamp with time zone,
  CONSTRAINT feeds_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
GRANT SELECT,INSERT,UPDATE,DELETE ON feeds TO mangafeedr_user;
GRANT USAGE,SELECT,UPDATE ON feeds_id_seq TO mangafeedr_user;

-- v1 to v2
ALTER TABLE feeds ADD COLUMN broken_link integer default 0;

UPDATE db_settings SET value = 2 WHERE key = 'schema_version';

-- v2 to v3
ALTER TABLE feeds ADD COLUMN deleted INTEGER DEFAULT 0;

UPDATE db_settings SET value = 3 WHERE key = 'schema_version';

-- v3 to v4
CREATE TABLE readlater(
	id serial NOT NULL,
	name text not null,
	link text null,
	CONSTRAINT readlater_pk PRIMARY KEY (id)
);
GRANT SELECT,INSERT,UPDATE,DELETE ON readlater TO mangafeedr_user;
GRANT USAGE,SELECT,UPDATE ON readlater_id_seq TO mangafeedr_user;

UPDATE db_settings SET value = 4 WHERE key = 'schema_version';

-- v4 to v5
ALTER TABLE feeds ADD COLUMN original_language_name TEXT;

UPDATE db_settings SET value = 5 WHERE key = 'schema_version';
