import datetime

import feedparser
import pytz

from parsers import Parser


class XMLParser(Parser.ParserInterface):
    def getLatestUpdateTimeFromFeed(self, feedTextOrFile: str) -> datetime:
        """Parse feed and return the date of the latest feed entry"""
        parsedFeed = feedparser.parse(feedTextOrFile)
        if parsedFeed.bozo == 1:
            raise IOError("RSS feed is malformed")
        latestDate = datetime.datetime(1970, 1, 1, 1, 1, 1, tzinfo=pytz.UTC)
        for item in parsedFeed.entries:
            convertedDate = self.convertDateStringToUtcDateTime(item.published)
            latestDate = max(convertedDate, latestDate)
        return latestDate
