import datetime

import pytz
from dateutil.parser import parse
from dateutil.parser.isoparser import tz


class ParserInterface:
    def getLatestUpdateTimeFromFeed(self, feedTextOrFile: str) -> datetime:
        """Load in the file for extracting text."""
        pass

    def convertDateStringToUtcDateTime(self, dateString: str) -> datetime:
        """Converts date from the currently loaded file."""
        parsedDate = parse(dateString)
        if parsedDate.tzinfo != tz.UTC:
            parsedDate = parsedDate.astimezone(pytz.UTC)
        return parsedDate
