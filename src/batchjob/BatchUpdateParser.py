import asyncio
from time import sleep
from typing import List, NamedTuple, Dict, Any

import aiohttp
from psycopg2.extras import NamedTupleCursor

from parsers import XMLParser, JsonParser
from logging_utils import setupLogging
from store.FeedRepository import setupDBConn

xmlParser = XMLParser.XMLParser()
jsonParser = JsonParser.JsonParser()
logger = setupLogging(loggerName="mangafeedr_batch", stdout=True, logfile=False)


# noinspection PyBroadException
async def _downloadAndParse(feed: NamedTuple, conn: Any, statusCounter: Dict[str, int]):
    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) \
        AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36',
               'accept': 'text/json,application/json;q=0.9,image/webp,image/apng,*/*;q=0.8',
               'accept-encoding': 'gzip, deflate, br',
               'accept-language': 'en-US,en',
               'cache-control': 'max-age=0',
               'upgrade-insecure-requests': '1',
               }
    async with aiohttp.ClientSession(raise_for_status=False, headers=headers,
                                     timeout=aiohttp.ClientTimeout(total=10)) as session:
        try:
            logger.debug(f"Calling {feed.link}")
            async with session.get(feed.link, ssl=False) as resp:
                if resp.status == 200:
                    content = await resp.text()
                    storedUpdateTime = feed.latest_update_date
                    if '.xml' in feed.link or 'rss' in feed.link:
                        latestFeedUpdateTime = xmlParser.getLatestUpdateTimeFromFeed(content)
                    else:
                        latestFeedUpdateTime = jsonParser.getLatestUpdateTimeFromFeed(content)

                    with conn.cursor() as cursor:
                        if storedUpdateTime is None or latestFeedUpdateTime > storedUpdateTime:
                            cursor.execute("""UPDATE feeds 
                                            SET latest_update_date = %(date)s, broken_link = 0 
                                            WHERE id = %(id)s""",
                                        {'date': latestFeedUpdateTime, 'id': feed.id})
                        else:
                            cursor.execute("UPDATE feeds SET broken_link = 0 WHERE id = %(id)s AND broken_link = 1",
                                        {'id': feed.id})
                    logger.debug(f"Done with request to {feed.link}")
                elif resp.status == 503:
                    retryAfter = resp.headers.get('Retry-After')
                    if retryAfter:
                        logger.info(f"{feed.link} return Retry-After: {retryAfter}")
                    statusCounter["503"] += 1
                elif resp.status == 401:
                    logger.warn("Got 401 response for feed %s with link %s", feed.id, feed.link)
                    statusCounter["401"] += 1
                else:
                    logger.error(f"Non-OK status code {resp.status} for url {feed.link}")
                    with conn.cursor() as cursor:
                        cursor.execute("UPDATE feeds SET broken_link = 1 WHERE id = %(id)s", {'id': feed.id})

        except asyncio.TimeoutError:
            logger.error(f"Timeout occurred for {feed.link}")
            statusCounter["timeout"] += 1
        except Exception as e:
            logger.error(f"Error occurred during batch handling of feed {feed.link}")
            logger.exception(e)
        finally:
            if session and not session.closed:
                await session.close()


def _runTasks(feeds: List[NamedTuple], conn: Any):
    statusCounter = {"503": 0, "timeout": 0, "401": 0}
    batchSize = 3
    loop = asyncio.get_event_loop()
    try:
        for i in range(0, len(feeds), batchSize):  # do work in batches to decrease server load
            if not len(feeds[i:i + batchSize]):
                break
            loop.run_until_complete(asyncio.gather(*[_downloadAndParse(feed, conn, statusCounter)
                                                     for feed in feeds[i:i + batchSize]]))
            sleep(1)
    finally:
        loop.close()
    logger.info(f"Total number of 401 responses: {statusCounter['401']}/{len(feeds)}")
    logger.info(f"Total number of 503 responses: {statusCounter['503']}/{len(feeds)}")
    logger.info(f"Total number of timeouts: {statusCounter['timeout']}/{len(feeds)}")


def _getListOfFeeds(conn: Any) -> List[NamedTuple]:
    with conn.cursor(cursor_factory=NamedTupleCursor) as cursor:
        cursor.execute("SELECT id, link, latest_update_date FROM feeds WHERE deleted = 0")
        return cursor.fetchall()


def runJobOnce():
    try:
        with setupDBConn() as conn:
            _runTasks(_getListOfFeeds(conn), conn)
    except Exception as e:
        logger.error("Fatal error occurred during batch job execution")
        logger.exception(e)
