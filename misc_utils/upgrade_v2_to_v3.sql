\c mangafeedrdb

ALTER TABLE feeds ADD COLUMN deleted INTEGER DEFAULT 0;

UPDATE db_settings SET value = 3 WHERE key = 'schema_version';
