\c mangafeedrdb

ALTER TABLE feeds ADD COLUMN original_language_name TEXT;

UPDATE db_settings SET value = 5 WHERE key = 'schema_version';
