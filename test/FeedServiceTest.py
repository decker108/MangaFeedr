import unittest
from datetime import datetime

import pytz
from hamcrest import assert_that, is_

from FeedService import FeedService
from model.Feed import Feed
from store.FeedRepository import FeedRepositoryInMemory


class Test(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        pass

    def setUp(self):
        self.repo = FeedRepositoryInMemory()  # TODO Replace with mock
        self.service = FeedService(self.repo)

    def tearDown(self):
        pass

    def testDecorateWithNewMarksCorrectFeedsAsNew(self):
        date1 = datetime(2013, 4, 5, 10, 11, 12)
        date2 = datetime(2013, 4, 6, 15, 27, 3)
        date3 = datetime(2013, 4, 7, 21, 30, 59)
        self.repo.store(Feed(channelTitle="feed1", channelLink="link1",
                             latestUpdateDate=date1, markedReadDate=date2))
        self.repo.store(Feed(channelTitle="feed2", channelLink="link2",
                             latestUpdateDate=date3, markedReadDate=date2))
        listOfFeeds = self.repo.getAll()

        feeds = self.service.calculateNewMarkersForFeeds(listOfFeeds)

        assert_that(len(feeds), is_(2))
        assert_that(feeds[0]['title'], is_("feed1"))
        assert_that(feeds[0]['new'], is_(False))
        assert_that(feeds[1]['title'], is_("feed2"))
        assert_that(feeds[1]['new'], is_(True))

    def testDecorateWithNewMarksCorrectFeedsAsNewForDB(self):
        self.repo.store(Feed(channelTitle="Feed A", channelLink="http://example.org/"))
        self.repo.store(Feed(channelTitle="Feed B", channelLink="http://example.org/",
                        latestUpdateDate=datetime.fromtimestamp(0, tz=pytz.UTC),
                        markedReadDate=datetime.now(tz=pytz.UTC)))
        listOfFeeds = self.repo.getAll()

        feeds = self.service.calculateNewMarkersForFeeds(listOfFeeds)

        assert_that(feeds[0]['title'], is_("Feed A"))
        assert_that(feeds[0]['new'], is_(True))
        assert_that(feeds[1]['title'], is_("Feed B"))
        assert_that(feeds[1]['new'], is_(False))


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testDecorateWithNewMarksCorrectFeedsAsNew']
    unittest.main()
