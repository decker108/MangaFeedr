# MangaFeedr
My homebrew replacement for the BriefRSS plugin for Firefox, which has been acting up lately.
Basically, I want to access my RSS feeds (which mostly contain manga) on-the-go and from either
a PC browser or a phone.

## Functionality:
* Easily add/remove feeds
* Show newly updated items
* Mark items as read
* Get feed updates in the background

## Dependencies
[requirements.txt](/requirements.txt)

## Docker setup
1. docker pull postgres:14.3-alpine
2. docker run -d \
        --name do-droplet-postgres \
        -e POSTGRES_PASSWORD=secret\
        -e PGDATA=/var/lib/postgresql/data/pgdata \
        -v $HOME/pgdata:/var/lib/postgresql/data \
        -p 5432:5432 \
        postgres:14.3-alpine
3. docker pull registry.gitlab.com/decker108/mangafeedr:latest
4. docker run -e IS_DOCKER=true -e MANGAFEEDR_CONFIG=/datasource.ini -p 8070:8070 -d registry.gitlab.com/decker108/mangafeedr:latest

For the batchjob, run the following:

1. docker pull registry.gitlab.com/decker108/mangafeedr:batchjob-v2.0
2. docker run --rm -e IS_DOCKER=true -e MANGAFEEDR_CONFIG=/datasource.ini \
   -v $HOME/mangafeedr/datasource.ini:/datasource.ini --network myNetwork \
   registry.gitlab.com/decker108/mangafeedr:batchjob-v2.0

## Manual setup
1. wget -O Mangafeedr.tar.gz https://gitlab.com/decker108/MangaFeedr/-/archive/master/MangaFeedr-master.tar.gz
2. tar -xzf MangaFeedr.tar.gz # optionally also: mv MangaFeedr-master MangaFeedr
3. virtualenv -p $(which python3.7) MangaFeedr/
4. cd MangaFeedr/
5. (optional) rm -rf test/
5. apt-get install postgresql postgresql-client
6. psql -U postgres -f misc_utils/upgrade_*
7. crontab -e # use the contents of misc_utils/cronjob
8. sh runserver.sh

(installation tested with Ubuntu server on DO)

## Howto run the BatchUpdateParserTest locally

Start a docker container on localhost:5432 by running:

     docker run --name mangafeedr-postgres -e POSTGRES_PASSWORD=blab -p 5432:5432 -d postgres

then set the MANGAFEEDR_CONFIG env var to the local_docker_db_config.ini file's path and run the test.
