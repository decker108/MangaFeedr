import datetime
import json

import pytz

from parsers import Parser


class JsonParser(Parser.ParserInterface):
    def getLatestUpdateTimeFromFeed(self, feedTextOrFile: str) -> datetime:
        """Parse feed and return the date of the latest feed entry"""
        if feedTextOrFile is None or len(feedTextOrFile) == 0:
            raise IOError("Got empty json string or file")
        parsedFeed = json.loads(feedTextOrFile)
        if parsedFeed['result'] != 'ok':
            raise IOError("Got non-OK JSON doc")
        latestDate = datetime.datetime(1970, 1, 1, 1, 1, 1, tzinfo=pytz.UTC)
        for item in parsedFeed['data']:
            convertedDate = self.convertDateStringToUtcDateTime(item['attributes']['createdAt'])
            latestDate = max(convertedDate, latestDate)
        return latestDate
