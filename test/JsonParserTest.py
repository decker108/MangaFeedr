import unittest
from datetime import datetime

import pytz

from parsers.JsonParser import JsonParser


class Test(unittest.TestCase):
    realFeed = r'test-resources/mangadex_v2_feed.json'

    def setUp(self):
        try:
            with open(self.realFeed):
                pass
        except IOError:
            self.realFeed = r'test/test-resources/mangadex_v2_feed.json'

    def testParseDateTimeStringReturnsDateTimeInUtc(self):
        dateString = "2021-02-01T23:59:58+00:00"
        convertedDate = JsonParser().convertDateStringToUtcDateTime(dateString)
        self.assertEqual(convertedDate, datetime(2021, 2, 1, 23, 59, 58, tzinfo=pytz.UTC))

    def testParseFeedToGetLatestUpdateDate(self):
        parser = JsonParser()
        with open(self.realFeed) as fd:
            jsonString = fd.read()
        expectedDate = datetime(2021, 8, 8, 10, 55, 43, tzinfo=pytz.UTC)

        actualDate = parser.getLatestUpdateTimeFromFeed(jsonString)

        self.assertEqual(actualDate, expectedDate)


if __name__ == "__main__":
    unittest.main()
