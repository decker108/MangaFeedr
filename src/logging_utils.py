import logging
from logging.handlers import RotatingFileHandler

import os


def setupLogging(loggerName: str, logfile: bool = True, stdout: bool = False, logLevel: int = logging.INFO,
                 logFileName: str = "app.log"):
    formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    isDocker = os.environ.get('IS_DOCKER', False) == 'true'

    logger = logging.getLogger(loggerName)
    logger.setLevel(logLevel)

    if logfile and not isDocker:
        logfilePath = os.path.join(os.environ.get('MANGAFEEDR_LOGPATH', '/var/log/mangafeedr/'), logFileName)
        rfh = RotatingFileHandler(logfilePath, maxBytes=1000000, backupCount=2)
        rfh.setFormatter(formatter)
        logger.addHandler(rfh)
    if stdout or isDocker:
        strh = logging.StreamHandler()
        strh.setFormatter(formatter)
        logger.addHandler(strh)

    return logger
