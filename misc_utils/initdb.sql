-- Run all these as the DB owner (e.g. postgres)
CREATE DATABASE mangafeedrdb;
CREATE ROLE mangafeedr_user WITH PASSWORD 'blab' LOGIN;
GRANT CONNECT ON DATABASE mangafeedrdb TO mangafeedr_user;

\c mangafeedrdb

CREATE TABLE db_settings(key text, value text);
INSERT INTO db_settings values('schema_version', 1);
