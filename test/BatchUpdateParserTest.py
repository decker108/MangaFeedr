import multiprocessing

import os
import unittest
from time import time, sleep
from configparser import ConfigParser

import psycopg2
import requests
from requests.exceptions import ConnectTimeout
from psycopg2.extras import NamedTupleCursor

from batchjob.BatchUpdateParser import runJobOnce


@unittest.skipIf(multiprocessing.cpu_count() == 1, 'Test requires at least 2 cores')
class Test(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.conn = cls._setupDbConn()
        cls.proc = multiprocessing.Process(target=mockServer)
        cls.proc.start()
        sleep(2)  # wait for server startup
        cls.waitForProcStart()

    @classmethod
    def tearDownClass(cls):
        cls.conn.close()
        cls.proc.terminate()
        sleep(0.1)
        if cls.proc.is_alive():
            cls.proc.close()

    @staticmethod
    def waitForProcStart():
        now = time()
        while time() - now < 1000:
            try:
                r = requests.get("http://localhost:5000/rss/1.xml", timeout=0.5)
                if r.status_code == 200:
                    return
                else:
                    sleep(0.5)
            except ConnectTimeout:
                pass
            except ConnectionRefusedError:
                pass
        raise RuntimeError("Failed to connect to mock server")

    @classmethod
    def _setupDbConn(cls):
        cfgParser = ConfigParser()
        cfgParser.read(os.environ.get("MANGAFEEDR_CONFIG"))
        addr = cfgParser.get('root', 'db.addr')
        portnum = cfgParser.get('root', 'db.port')
        db_name = cfgParser.get('root', 'db.name')
        username = cfgParser.get('root', 'db.username')
        passw = cfgParser.get('root', 'db.password')
        conn = psycopg2.connect(database=db_name, host=addr,
                                user=username, password=passw, port=portnum)
        conn.autocommit = True
        return conn

    def testHappyPath(self):
        with self.conn.cursor() as cursor:
            cursor.execute("delete from feeds")
            cursor.execute("insert into feeds(name, link) values(%(name)s, %(link)s)",
                           {"name": "Feed A", "link": "http://localhost:5000/rss/1.xml"})
            cursor.execute("insert into feeds(name, link) values(%(name)s, %(link)s)",
                           {"name": "Feed B", "link": "http://localhost:5000/rss/2.xml"})

        runJobOnce()

        with self.conn.cursor(cursor_factory=NamedTupleCursor) as cursor:
            cursor.execute("""
                select name, link, latest_update_date at time zone 'UTC' as latest_update_date
                from feeds order by name
            """)
            feeds = cursor.fetchall()

        self.assertEqual(feeds[0].name, "Feed A")
        self.assertEqual(feeds[0].link, "http://localhost:5000/rss/1.xml")
        self.assertEqual(feeds[0].latest_update_date.strftime("%Y-%m-%dT%H:%M:%S"), "2018-04-01T15:25:14")
        self.assertEqual(feeds[1].name, "Feed B")
        self.assertEqual(feeds[1].link, "http://localhost:5000/rss/2.xml")
        self.assertEqual(feeds[1].latest_update_date.strftime("%Y-%m-%dT%H:%M:%S"), "2018-12-22T01:04:07")


def mockServer():
    from flask import Flask, make_response

    app = Flask(__name__)

    def loadRssFile(filepath, altFilepath):
        try:
            with open(filepath, "rt") as fd:
                text = fd.read()
                return text
        except FileNotFoundError:
            with open(altFilepath, "rt") as fd:
                text = fd.read()
                return text

    rss1 = loadRssFile('test/test-resources/mangadex_rss.xml', 'test-resources/mangadex_rss.xml')
    rss2 = loadRssFile('test/test-resources/mangadex_rss2.xml', 'test-resources/mangadex_rss2.xml')

    @app.route("/rss/1.xml")
    def feed1():
        resp = make_response(rss1)
        resp.headers['Content-Type'] = 'application/xml'
        return resp

    @app.route("/rss/2.xml")
    def feed2():
        resp = make_response(rss2)
        resp.headers['Content-Type'] = 'application/xml'
        return resp

    app.run(port=5000)


if __name__ == "__main__":
    unittest.main()
