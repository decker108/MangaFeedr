\c mangafeedrdb

CREATE TABLE readlater(
	id serial NOT NULL, 
	name text not null, 
	link text null, 
	CONSTRAINT readlater_pk PRIMARY KEY (id)
);
GRANT SELECT,INSERT,UPDATE,DELETE ON readlater TO mangafeedr_user;
GRANT USAGE,SELECT,UPDATE ON readlater_id_seq TO mangafeedr_user;

UPDATE db_settings SET value = 4 WHERE key = 'schema_version';
