import unittest

from hamcrest import assert_that, is_, not_none

from model.Feed import Feed
from store.FeedRepository import FeedRepositoryInMemory


class Test(unittest.TestCase):

    def setUp(self):
        self.repo = FeedRepositoryInMemory()  # TODO replace with real impl

    def tearDown(self):
        self.repo.entities = []

    def testStoreFeed(self):
        feed = Feed(channelTitle="title1", channelLink="link1")
        feedId = self.repo.store(feed)
        storedFeed = self.repo.get(feedId)
        self.assertEqual(feed, storedFeed)

    def testGetSeriesReturnsValidSeries(self):
        feed = Feed(channelTitle="title2", channelLink="link2")
        feedId = self.repo.store(feed)
        assert_that(feedId, not_none())
        storedFeed = self.repo.get(feedId)
        assert_that(storedFeed.id, is_(feedId))
        assert_that(storedFeed.channelTitle, is_("title2"))
        assert_that(storedFeed.channelLink, is_("link2"))

    def testGetAllFeeds(self):
        self.repo.store(Feed(channelTitle="title1", channelLink="link1"))
        self.repo.store(Feed(channelTitle="title2", channelLink="link2"))
        self.repo.store(Feed(channelTitle="title3", channelLink="link3"))
        feeds = self.repo.getAll()
        self.assertEqual(len(feeds), 3)
        assert_that(feeds[0]['name'], is_("title1"))
        assert_that(feeds[1]['name'], is_("title2"))
        assert_that(feeds[2]['name'], is_("title3"))


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testGetSeriesReturnsValidSeries']
    unittest.main()
