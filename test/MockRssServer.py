from flask import Flask, make_response

app = Flask(__name__)


def loadRssFile(filepath):
    with open(filepath, "rt") as fd:
        text = fd.read()
        return text


rss1 = loadRssFile('test-resources/mangadex_rss.xml')
rss2 = loadRssFile('test-resources/mangadex_rss2.xml')


@app.route("/rss/1")
def feed1():
    resp = make_response(rss1)
    resp.headers['Content-Type'] = 'application/xml'
    return resp


@app.route("/rss/2")
def feed2():
    resp = make_response(rss2)
    resp.headers['Content-Type'] = 'application/xml'
    return resp
