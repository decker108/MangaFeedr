FROM python:3.9-alpine

LABEL maintainer="Decker108"

COPY requirements.txt /

RUN apk add --no-cache python3-dev gcc musl-dev make libffi-dev build-base postgresql-dev postgresql-libs && pip install -r requirements.txt && apk del libffi-dev build-base postgresql-dev python3-dev gcc musl-dev make

COPY src/ /src/
COPY datasource.ini /

# Required for gevent 1.5.0 for py 3.9+
ENV PURE_PYTHON=1

CMD ["python", "/src/start_server.py", "8070"]
