class Feed:

    def __init__(self, id=None, channelTitle=None, channelLink=None,
                 latestUpdateDate=None, markedReadDate=None, originalTitle=None):
        self.id = id
        self.markedReadDate = markedReadDate
        self.latestUpdateDate = latestUpdateDate
        self.channelTitle = channelTitle
        self.channelLink = channelLink
        self.originalTitle = originalTitle
        self.brokenLink = 0

    def __str__(self):
        return "".join(["Title: ",
                        str(self.channelTitle),
                        " Link: ",
                        str(self.channelLink)])
