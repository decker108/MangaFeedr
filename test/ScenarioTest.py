import unittest
from configparser import ConfigParser
from datetime import datetime

import os
import psycopg2
import pytz
from hamcrest import assert_that, is_
from psycopg2.extras import NamedTupleCursor

from parsers.XMLParser import XMLParser
from model.Feed import Feed
from store.FeedRepository import FeedRepository


class Test(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.conn = cls._setupDbConn()

    @classmethod
    def _setupDbConn(cls):
        cfgParser = ConfigParser()
        cfgParser.read(os.environ.get("MANGAFEEDR_CONFIG"))
        addr = cfgParser.get('root', 'db.addr')
        portnum = cfgParser.get('root', 'db.port')
        db_name = cfgParser.get('root', 'db.name')
        username = cfgParser.get('root', 'db.username')
        passw = cfgParser.get('root', 'db.password')
        conn = psycopg2.connect(database=db_name, host=addr,
                                user=username, password=passw, port=portnum)
        conn.autocommit = True
        return conn

    @classmethod
    def tearDownClass(cls):
        with cls.conn.cursor(cursor_factory=NamedTupleCursor) as cursor:
            cursor.execute("delete from feeds")
        cls.conn.close()

    def testRefreshAndReadFeed(self):
        feedRepo = FeedRepository()
        testFeed = Feed(channelTitle="manga1", channelLink=r'test/test-resources/realrss.xml',
                        latestUpdateDate=datetime(2012, 12, 1, 1, 1, 1, tzinfo=pytz.utc))

        lastUpdate = XMLParser().getLatestUpdateTimeFromFeed(testFeed.channelLink)
        expectedDate = datetime(2013, 2, 3, 4, 52, 4, tzinfo=pytz.UTC)
        self.assertEqual(lastUpdate, expectedDate)
        feedId = feedRepo.store(testFeed)
        feedRepo.markFeedRead(feedId)
        markedReadDate = feedRepo.get(feedId)['marked_read_date']
        self.assertIsNotNone(markedReadDate)

    def testRemoveFeed(self):
        self.repo = FeedRepository()
        feed = Feed(channelTitle="title1", channelLink="link1",
                    latestUpdateDate="Sat, 05 Feb 2013 23:21:04 -0500")
        feedId = self.repo.store(feed)
        self.repo.delete(feedId)
        storedFeed = self.repo.get(feedId)
        assert_that(storedFeed['deleted'], is_(1))


if __name__ == "__main__":
    unittest.main()
