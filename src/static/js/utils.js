function validateRSSLink(url) {
	console.log("Validating URL");
	if (url == undefined || url == null) {
		return false;
	}
	if (url.indexOf('.xml') != -1 || url.indexOf('.rss') != -1) {
		return false;
	}
	return true;
}

function hiliteOnHover() {
	document.querySelector('ul li').mouseover(function() {
        document.querySelector(this).addClass('hovered');
    }).mouseout(function() {
        document.querySelector(this).removeClass('hovered');
    });
}

function guessNameFromRssUrl(inputUrl) {
	if (!inputUrl) {
		return;
	}
	const url = "" + inputUrl;
	if (inputUrl.includes('mangafox') || inputUrl.includes('fanfox')) {
        const normalizedUrl = url.replace('https://', '')
                                                    .replace('http://', '')
                                                    .replace('mangafox.la/rss/','')
                                                    .replace('fanfox.net/rss/','')
                                                    .replace('.xml','')
                                                    .replace('.rss','')
                                                    .replace(/_/g,' ');
        const capitalizedUrl = normalizedUrl[0].toUpperCase() + normalizedUrl.substring(1);
        if (!capitalizedUrl || capitalizedUrl.length === 0) {
            return undefined;
        }
        return capitalizedUrl;
	} else {
        let normalizedUrl = url.replace('https://mangadex.org/title/', '').split('/');
        const splitIndex = (normalizedUrl.length > 1) ? 1 : 0;
        normalizedUrl = normalizedUrl[splitIndex].replace(/-/g, ' ');
        const capitalizedUrl = normalizedUrl[0].toUpperCase() + normalizedUrl.substring(1);
        if (!capitalizedUrl || capitalizedUrl.length === 0) {
            return undefined;
        }
        return capitalizedUrl;
	}
	return undefined;
}

function guessLinkFromUrl(inputUrl) {
    if (!inputUrl) {
        return undefined;
    }
    const mangaId = inputUrl.replace('https://mangadex.org/title/', '').split('/')[0];
    return `https://api.mangadex.org/manga/${mangaId}/feed?translatedLanguage[]=en&order[createdAt]=desc&limit=5`;
}

function switchLanguage() {
    document.querySelectorAll('.manga-title').forEach((el) => {
        const alphaTitle = el.getAttribute('data-english-title');
        const origTitle = el.getAttribute('data-original-title');
        if (el.innerHTML === alphaTitle && origTitle && origTitle !== 'None') {
            el.innerHTML = origTitle;
        } else {
            el.innerHTML = alphaTitle;
        }
    })
}
