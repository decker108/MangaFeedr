from sys import exc_info

from flask import Flask, render_template, redirect, url_for, request, make_response

from FeedService import FeedService
from logging_utils import setupLogging
from model.InvalidFeedDataException import InvalidFeedDataException
from store.FeedRepository import FeedRepository

app = Flask(__name__)
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 600  # seconds
service = FeedService(FeedRepository())
logger = setupLogging("mangafeedr_server")


@app.route('/index/')
def rootRoute():
    return index()


@app.route('/mangafeedr/')
def mangafeedr():
    return index()


@app.route('/')
def index():
    errorParam = request.form.get('error')
    hideReadParam = request.form.get('hideread')
    feedIdParam = request.form.get('feedid')
    storedFeeds = service.calculateNewMarkersForFeeds(service.getAllFeeds())
    response = make_response(
        render_template('index.html', feeds=storedFeeds, error=errorParam, feedid=feedIdParam, hideRead=hideReadParam),
        200)
    response.headers['Cache-Control'] = 'no-store'
    return response


@app.route('/addfeedpagelink/', methods=['GET', 'POST'])
def addfeedpagelink():
    return redirect(url_for('addfeed'))


@app.route('/addfeed', methods=['GET', 'POST'])
def addfeed():
    try:
        if request.form.get('error') is not None:
            return render_template('addfeed.html', error=True,
                                   errormsg=request.form.get('errormsg'))
        return render_template('addfeed.html')
    except Exception as e:
        logger.error("Unexpected error when adding feed")
        logger.exception(e)
        return render_template('addfeed.html', error=True,
                               errormsg="Unexpected error when adding feed")


@app.route('/removefeed', methods=['POST'])
def removefeed():
    feedId = int(request.form.get('feedid'))
    try:
        logger.info(f"Deleting feed with id {feedId}")
        service.deleteFeed(feedId)
    except Exception as e:
        logger.error("Unexpected error when removing feed")
        logger.exception(e)
        response = make_response(render_template('index.html', errormsg="Unexpected error when removing feed",
                                                 error=True, feedid=feedId), 200)
        response.headers['Cache-Control'] = 'no-store'
        return response
    return redirect(url_for('index'))


@app.route('/addfeedsubmit', methods=['POST'])
def addfeedsubmit():
    try:
        title = request.form.get('title')
        url = request.form.get('url')
        originalTitle = request.form.get('orig-title')
        service.storeFeed(title, url, originalTitle)
        logger.debug(msg="Adding new feed with name {} and url {}".format(title, url))
    except InvalidFeedDataException:
        return render_template('addfeed.html', error=True, errormsg=exc_info()[1].msg)
    except Exception as e:
        logger.error("Unexpected error when submitting feed")
        logger.exception(e)
        return render_template('addfeed.html', error=True, errormsg="Unexpected error")
    return redirect(url_for('index'))


@app.route('/refreshfeeds/')
def refreshfeeds():
    return redirect(url_for('index'))


@app.route('/markread', methods=['POST'])
def markread():
    feedId = int(request.form.get('id'))
    service.markFeedRead(feedId)
    return redirect(url_for('index'))


def addreadlaterentry(params):
    try:
        service.addReadLaterEntry(params.get('name'), params.get('url'))
    except Exception as e:
        logger.error("Unexpected error when adding read later entry")
        logger.exception(e)
        return render_template('readlater.html', error=True, errormsg="Unexpected error")
    return redirect(url_for('readlater'))


@app.route('/readlater/', methods=['GET', 'POST'])
def readlater():
    if request.form.get('name') is not None:
        formParams = {'name': request.form.get('name'), 'url': request.form.get('url')}
        return addreadlaterentry(formParams)
    else:
        readlaterentries = service.getAllReadLaterEntries()
        errorParam = request.form.get('error')
        return render_template('readlater.html', entries=readlaterentries,
                               deleteSuccess=None, entryid=None, errormsg=errorParam)
