docker build -f Dockerfile -t decker108/mangafeedr:1.7-alpine .

docker run -e IS_DOCKER='true' -e MANGAFEEDR_CONFIG=/datasource.ini -p 8070:8070 decker108/mangafeedr:1.7-alpine

docker login -u $PERSONAL_ACCESS_TOKEN -p $PERSONAL_ACCESS_TOKEN registry.digitalocean.com

docker push registry.digitalocean.com/neuromancer-hacked-jp/decker108/mangafeedr:1.7-alpine
