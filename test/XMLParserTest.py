import unittest
from datetime import datetime

import pytz

from parsers.XMLParser import XMLParser


class Test(unittest.TestCase):
    mockFeed = r'test-resources/testrss2.xml'
    realFeed = r'test-resources/realrss.xml'

    def setUp(self):
        try:
            with open(self.mockFeed):
                pass
            with open(self.realFeed):
                pass
        except IOError:
            self.mockFeed = r'test/test-resources/testrss2.xml'
            self.realFeed = r'test/test-resources/realrss.xml'

    def testParseDateTimeStringReturnsDateTimeInUtc(self):
        dateString = "Sat, 02 Feb 2013 23:51:04 -0500"
        testDate = XMLParser().convertDateStringToUtcDateTime(dateString)
        self.assertEqual(testDate, datetime(2013, 2, 3, 4, 51, 4, tzinfo=pytz.UTC))

    def testParseFeedToGetLatestUpdateDate(self):
        parser = XMLParser()

        date1 = parser.getLatestUpdateTimeFromFeed(self.realFeed)
        expectedDate1 = datetime(2013, 2, 3, 4, 52, 4, tzinfo=pytz.UTC)
        date2 = parser.getLatestUpdateTimeFromFeed(self.mockFeed)
        expectedDate2 = datetime(2013, 2, 6, 6, 1, 1, tzinfo=pytz.UTC)

        self.assertEqual(date1, expectedDate1)
        self.assertEqual(date2, expectedDate2)


if __name__ == "__main__":
    unittest.main()
