import os
from uuid import uuid4
import psycopg2
import pytz
from psycopg2.extras import DictCursor
from configparser import ConfigParser
from datetime import datetime


def setupDBConn():
    cfgParser = ConfigParser()
    cfgParser.read(os.environ['MANGAFEEDR_CONFIG'])
    addr = cfgParser.get('root', 'db.addr')
    portnum = cfgParser.get('root', 'db.port')
    db_name = cfgParser.get('root', 'db.name')
    username = cfgParser.get('root', 'db.username')
    passw = cfgParser.get('root', 'db.password')
    conn = psycopg2.connect(database=db_name, host=addr,
                            user=username, password=passw, port=portnum,
                            cursor_factory=DictCursor)
    conn.autocommit = True
    return conn


class FeedRepository:
    """The repository for series, storing series names, authors, vintages and latest update times."""

    def store(self, feed):
        with setupDBConn() as conn:
            with conn.cursor() as cursor:
                values = {
                    'name': feed.channelTitle,
                    'link': feed.channelLink,
                    'orig_name': feed.originalTitle
                }
                cursor.execute("""
                INSERT INTO feeds (name, link, original_language_name) 
                VALUES (%(name)s, %(link)s, %(orig_name)s) 
                RETURNING id""", values)
                feedId = cursor.fetchone()[0]
                return feedId

    def delete(self, feedId):
        with setupDBConn() as conn:
            with conn.cursor() as cursor:
                cursor.execute("UPDATE feeds SET deleted = 1 WHERE id = %(feedId)s", {'feedId': feedId})

    def get(self, feedId):
        with setupDBConn() as conn:
            with conn.cursor() as cursor:
                cursor.execute("SELECT * FROM feeds WHERE id = %(feedId)s", {'feedId': feedId})
                return cursor.fetchone()

    def getAll(self):
        with setupDBConn() as conn:
            with conn.cursor() as cursor:
                cursor.execute("""
                    SELECT id, broken_link, name, link, 
                           latest_update_date at time zone 'UTC' as latest_update_date,
                           marked_read_date at time zone 'UTC' as marked_read_date,
                           original_language_name
                    FROM feeds WHERE deleted = 0 ORDER BY latest_update_date DESC
                """)
                return cursor.fetchall()

    def markFeedRead(self, feedId):
        if feedId is not None:
            with setupDBConn() as conn:
                with conn.cursor() as cursor:
                    cursor.execute("UPDATE feeds SET marked_read_date = %(date)s WHERE id = %(feedId)s",
                                   {'date': datetime.now(tz=pytz.UTC), 'feedId': feedId})

    def setLatestUpdateDate(self, storedDate, latestDate, feedId):
        if storedDate is None or latestDate > storedDate:
            with setupDBConn() as conn:
                with conn.cursor() as cursor:
                    cursor.execute("UPDATE feeds SET latest_update_date = %(date)s WHERE id = %(feedId)s",
                                   {'date': latestDate, 'feedId': feedId})

    def checkForDuplicateFeed(self, url):
        with setupDBConn() as conn:
            with conn.cursor() as cursor:
                cursor.execute("SELECT COUNT(id) AS idcount FROM feeds WHERE link = %(url)s", {'url': url})
                result = cursor.fetchone()['idcount']
                return result >= 1

    def getListOfFeedsForUpdateCheck(self):  # TODO use FeedService method instead
        with setupDBConn() as conn:
            with conn.cursor() as cursor:
                cursor.execute("SELECT id, link, latest_update_date FROM feeds WHERE deleted = 0")
                return cursor.fetchall()

    def setLinkBrokenStatusForFeed(self, feedId, brokenStatus):
        with setupDBConn() as conn:
            with conn.cursor() as cursor:
                cursor.execute("""UPDATE feeds 
                                SET broken_link = %(brokenLink)s 
                                WHERE id = %(id)s 
                                AND broken_link != %(brokenLink)s""",
                               {'id': feedId, 'brokenLink': 1 if brokenStatus else 0})

    def getReadLaterEntries(self):
        with setupDBConn() as conn:
            with conn.cursor() as cursor:
                cursor.execute("SELECT * FROM readlater ORDER BY name")
                return cursor.fetchall()

    def addReadLaterEntry(self, name, link):
        with setupDBConn() as conn:
            with conn.cursor() as cursor:
                cursor.execute("INSERT INTO readlater(name, link) VALUES(%(name)s, %(link)s)",
                               {'name': name, 'link': link})


class FeedRepositoryInMemory(FeedRepository):

    def __init__(self):
        super().__init__()
        self.entities = []

    def store(self, feed):
        feed.id = uuid4()
        self.entities.append(feed)
        return feed.id

    def delete(self, feedId):
        for feed in self.entities:
            if feed.id == feedId:
                self.entities.remove(feed)
                break

    def get(self, feedId):
        for feed in self.entities:
            if feed.id == feedId:
                return feed
        return None

    def getAll(self):
        feeds = []
        for feed in self.entities:
            feeds.append({
                'id': feed.id,
                'name': feed.channelTitle,
                'link': feed.channelLink,
                'marked_read_date': feed.markedReadDate,
                'latest_update_date': feed.latestUpdateDate,
                'broken_link': feed.brokenLink,
            })
        return feeds

    def markFeedRead(self, feedId):
        date = datetime.now()
        for feed in self.entities:
            if feed.id == feedId:
                feed.markedReadDate = date
                break

    def setLatestUpdateDate(self, storedDate, latestDate, feedId):
        for feed in self.entities:
            if (storedDate is None or latestDate > storedDate) and feed.id == feedId:
                feed.latestUpdateDate = latestDate
                break

    def checkForDuplicateFeed(self, url):
        for feed in self.entities:
            if feed.channelLink == url:
                return True
        return False

    def getReadLaterEntries(self):
        return None

    def addReadLaterEntry(self, name, link):
        pass
