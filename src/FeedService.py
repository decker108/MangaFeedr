from model.InvalidFeedDataException import InvalidFeedDataException
from model.Feed import Feed


class FeedService:
    """Service for Feeds and related metadata"""

    def __init__(self, repo):
        self.repo = repo

    @staticmethod
    def calculateNewMarkersForFeeds(listOfFeeds):
        outputList = []
        for feed in listOfFeeds:
            if feed['marked_read_date'] is None:
                new = True
            elif feed['latest_update_date'] is None:
                new = False
            else:
                new = True if feed['marked_read_date'] < feed['latest_update_date'] else False
            broken = feed['broken_link'] == 1
            outputList.append({'id': feed['id'], 'title': feed['name'], 'new': new,
                               'broken': broken, 'update_date': feed['latest_update_date'],
                               'orig_title': feed.get('original_language_name', None)})
        return outputList

    def validateFeed(self, title, url):
        if len(title) <= 0:
            raise InvalidFeedDataException("Title is empty")
        if len(url) <= 0:
            raise InvalidFeedDataException("URL is empty")
        if self.repo.checkForDuplicateFeed(url):
            raise InvalidFeedDataException("Feed already exists")
        return True

    def getAllFeeds(self):
        return self.repo.getAll()

    def getFeed(self, feedId):
        if feedId is None or feedId <= 0:
            raise ValueError("Feed id was null or negative")
        return self.repo.get(feedId)

    def storeFeed(self, title, url, originalTitle):
        self.validateFeed(title, url)
        self.repo.store(Feed(channelTitle=title, channelLink=url, originalTitle=originalTitle))

    def deleteFeed(self, feedId):
        if feedId is None or feedId <= 0:
            raise ValueError("Feed id was null or negative")
        self.repo.delete(feedId)

    def markFeedRead(self, feedId):
        if feedId is None or feedId <= 0:
            raise ValueError("Feed id was null or negative")
        self.repo.markFeedRead(feedId)

    def getAllReadLaterEntries(self):
        return self.repo.getReadLaterEntries()

    def addReadLaterEntry(self, name, url):
        if name is None or name == '':
            raise ValueError("Name was null or empty")
        self.repo.addReadLaterEntry(name, url)
