\c mangafeedrdb

ALTER TABLE feeds ADD COLUMN broken_link integer default 0;

UPDATE db_settings SET value = 2 WHERE key = 'schema_version';