-- Table: feeds

-- DROP TABLE feeds;
\c mangafeedrdb
CREATE TABLE feeds
(
  id serial NOT NULL,
  name text,
  link text,
  marked_read_date timestamp with time zone,
  latest_update_date timestamp with time zone,
  CONSTRAINT feeds_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
GRANT SELECT,INSERT,UPDATE,DELETE ON feeds TO mangafeedr_user;
GRANT USAGE,SELECT,UPDATE ON feeds_id_seq TO mangafeedr_user;
