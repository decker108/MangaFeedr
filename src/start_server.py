from sys import argv
from gevent.pywsgi import WSGIServer
from Controller import app
from batchjob.BatchUpdateParser import runJobOnce


def main():
    if len(argv) > 1 and argv[1] == '--batch-job-only':
        runJobOnce()
    else:
        port = int(argv[1]) if len(argv) > 1 else 8070
        http_server = WSGIServer(('', port), app)
        http_server.serve_forever()


if __name__ == '__main__':
    # Set the envvar PURE_PYTHON=1 to run gevent in py 3.9+
    main()
